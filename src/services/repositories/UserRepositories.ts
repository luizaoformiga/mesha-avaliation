type User = {
    email: string;
    password: string;
}

export default interface LoginInterface extends User {
    getLogin(email: string, password: string): Promise<User> | any;
    putLogin(email: string, password: string): Promise<User> | any;
    postLogin(email: string, password: string): Promise<User> | any;
    deleteLogin(email: string, password: string): Promise<User> | any;
}