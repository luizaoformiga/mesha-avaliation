import UserModel from '../../app/models/userModel';
import UserInterface from '../repositories/UserRepositories';

export default class UserContracts implements UserInterface {
    email: string;
    password: string;
    
    async getLogin(email: string, password: string) {
      const data =  { email, password }
      const response = await UserModel.find(data);
      return response;
    }

    async putLogin(email: string, password: string) {
      const data =  { email, password }
      const response = await UserModel.update(data);
      return response;
    }

    async postLogin(email: string, password: string) {
      const data =  { email, password }
      const response = await UserModel.create(data);
      return response;
    }

    async deleteLogin(email: string, password: string) {
      const data =  { email, password }
      const response = await UserModel.remove(data);
      return response;
    }
}