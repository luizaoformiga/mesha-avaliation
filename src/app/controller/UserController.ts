import { Request, Response } from 'express';
import UserContracts from '../../services/contracts/UserContracts';

export default class UserController extends UserContracts {
  async post(req: Request, res: Response) {
    try {
      const [email, password]: string = req.body;
      const user = await this.getLogin(email, password);

      if(!user) return res.status(400).json({ message: "NOT FOUND" });
      
      return res.status(200).json(user); 

    } catch (error) {
      return res.status(403).json(error);  
    }  
  }
    
  async get(req: Request, res: Response) {
    try {
      const [email, password]: string = req.body;
      const user = await this.postLogin(email, password);
      
      if(!user) return res.status(404).json({ message: "NOT FOUND" });

      return res.status(200).json(user);

    } catch (error) {
      return res.status(404).json(error);
    }
  }

  async put(req: Request, res: Response) {
    try {
      const [email, password]: string = req.body;
      const user = await this.putLogin(email, password);
      
      if(!user) return res.status(401).json({ message: "NOT FOUND" });

      return res.status(201).json(user);

    } catch (error) {
      return res.status(500).json(error);
    }
  }

  async delete(req: Request, res: Response) {
    try {
      const [email, password]: string = req.body;
      const user = await this.deleteLogin(email, password);
      
      if(!user) return res.status(400).json({ message: "NOT FOUND" });

      return res.status(201).json(user);
      
    } catch (error) {
      return res.status(500).json(error);
    }   
  }
}


