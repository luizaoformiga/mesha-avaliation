import Mongoose from 'mongoose';

const schema = new Mongoose.Schema({
    email: {
        type: String,
        required: [true, 'Email is required'],
        unique: true,
        lowercase: true,
        trim: true
    }, 
    password: {
        type: String,
        required: [true, 'Password is required'],
        select: false
    },     
},   {
    timestamps: { createdAt: true, updatedAt: true },
    toJSON: { virtuals: true, getters: true,
        transform(document: any, returns: any) {
            returns.id = returns._id
            delete returns._id
        }
    },
    toObject: { virtuals: true, getters: true },
    versionKey: false 
})

const UserModel = Mongoose.model('Users', schema);

export default UserModel;