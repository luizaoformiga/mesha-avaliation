### Mesha Avaliation Fullstack Nodejs

**Para instalar as dependências:**

```
npm i 
```

**Para iniciar o projeto:**

```
npm start
```

#### Do que se trata esse sistema?

Apenas uma arquitetura em MVC básica, sem tela, apenas um breve exemplo com refatoração nos controllers para melhor manutenção e boas práticas do código. Também foi usado o Typescript para melhor tipagem digamos. Banco de dados foi usado um modelo noSQL (mongoDB).

Feito com muito carinho por Luiz Lima
